Prob.4:
n! means n × (n - 1) × ... × 3 × 2 × 1
Eg:
8! = 40320
9! = 362880
10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
The sum of the digits in the numbers are:
digitSum(8!) = 4 + 0 + 3 + 2 + 0 = 9
digitSum(9!) = 3 + 6 + 2 + 8 + 8 + 0  = 27
digitSum(10!) = 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27
 
Given a series of numbers 1!..n! find and print the longest sequence of numbers where the digit sum for their factorials does not increase.
digitSum(x!) <= digitSum((x+1)!) .. <= digitSum((x+k))
Prints: x, x+1 .. x+k